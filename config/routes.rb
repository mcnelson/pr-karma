Rails.application.routes.draw do
  resource :github_hook, only: :create, defaults: {formats: :json}, path: 'github-hook'
  resources :levels, only: :index
  resources :pull_request_actions, only: :index, path: 'pull-requests'
end
