import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
Vue.prototype.$store = Vuex

const store = new Vuex.Store({
  state: () => {
    return {
      levels: [],
      pullRequestActions: []
    }
  },
  getters: {
    levels: state => state.levels,
    handles: (state) => {
      var uniqueHandles = []
      state.levels.forEach(level => {
        if (!uniqueHandles.includes(level.handle)) {
          uniqueHandles.push(level.handle)
        }
      })

      return uniqueHandles
    },
    pullRequestActions: state => state.pullRequestActions,
    pullRequestActionsByRepo: (state) => {
      var grouped = {}
      state.pullRequestActions.forEach(action => {
        // Wow, no hammer operator?
        if (!grouped[action.repo]) {
          grouped[action.repo] = []
        }

        grouped[action.repo].push(action)
      })

      return grouped
    }
  },
  mutations: {
    levels (state, value) {
      state.levels = value
    },
    pullRequestActions (state, value) {
      state.pullRequestActions = value
    }
  }
})

export default store
