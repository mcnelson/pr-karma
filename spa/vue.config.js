const path = require('path');

module.exports = {
  configureWebpack: (config) => {
    // config.output.filename = '../../public/spa.js'
    // config.output.chunkFilename = '../../public/[name].js' // Hacky?
    // config.optimization.splitChunks.cacheGroups.vendors.name = 'vendors'
  },
  chainWebpack: config => {
    config.module.rule('svg').use('file-loader').loader('html-loader')
  },
};
