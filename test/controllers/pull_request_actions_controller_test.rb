require 'test_helper'

class PullRequestActionsControllerTest < ActionDispatch::IntegrationTest
  test 'fetches current pull requests titles and rough idea of the adjustment' do
    example_handle = karma_events(:karma_event_0).handle

    get polymorphic_url([:pull_request_actions], handle: example_handle), as: :json

    assert_response :success

    assert_kind_of Array, json

    first_item = json.first
    assert_match /Change this/, first_item.pullRequestTitle

    assert_equal "medium", first_item.change # WIP
    assert_equal 1, first_item.changeRelativeAmount
    assert_equal true, Time.zone.parse(first_item.at).utc?
  end
end
