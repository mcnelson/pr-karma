require 'test_helper'

class PullRequestReviewJobTest < ActiveSupport::TestCase
  test "persists the event and sends one message" do
    5.times do
      KarmaEvent.create!(repo_name: 'bla',
                         adjustment: 1,
                         event: :review_comment,
                         created_at: 30.minutes.ago,
                         review_id: 123,
                         handle: 'foo')
    end

    payload = {action: 'submitted',
               pull_request: {title: 'yoo', head: {repo: {name: 'bar'}}, user: {login: 'bar'}},
               review: {id: 123, state: 'commented', user: {login: 'foo'}, body: 'bar'}}

    job = PullRequestReviewJob.new
    job.perform(payload)

    assert_equal "foo remains at *In balance* for reviewing pull request 'yoo' with 5 comments.", job.slack_message

    ke = KarmaEvent.last
    assert_not_empty ke.review_comments
  end

  test "aborts the job if the handler returned nil" do
    payload = {action: 'submitted',
               pull_request: {title: 'yoo', head: {repo: {name: 'bar'}}, user: {login: 'foo'}},
               review: {id: 123, state: 'commented', user: {login: 'foo'}}}

    job = PullRequestReviewJob.new

    assert_nothing_raised do
      job.perform(payload)
    end

    assert_nil job.slack_message
  end
end
