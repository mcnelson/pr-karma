require 'test_helper'

class PullRequestReviewHandlerTest < ActiveSupport::TestCase
  test 'happy path PR review submitted' do
    json = payload_mock 'github_pull_request_review'

    handler = PullRequestReviewHandler.new(json)

    assert_difference -> { KarmaEvent.count } => 1 do
      handler.handle
    end

    assert_nil handler.slack_message

    ke = KarmaEvent.last
    assert_equal 2, ke.adjustment
    assert_equal 'review_submitted', ke.event
    assert_equal 'prksandbox', ke.repo_name
    assert_equal 'mrinterweb', ke.handle
    assert_equal 'Update README.md', ke.pr_title
    assert_equal 146972580, ke.review_id
    assert_equal 208947793, ke.pr_id
  end

  test 'happy path PR approved' do
    json = payload_mock 'github_pull_request_approved'
    handler = PullRequestReviewHandler.new(json)

    assert_difference -> { KarmaEvent.count } => 1 do
      handler.handle
    end

    assert_nil handler.slack_message

    ke = KarmaEvent.last
    assert_equal 4, ke.adjustment
    assert_equal 'review_approved', ke.event
    assert_equal 'prksandbox', ke.repo_name
    assert_equal 'Update README.md', ke.pr_title
  end

  test 'does nothing if the approval was from the author of the PR' do
    json = payload_mock 'github_pull_request_approved_same_author'
    handler = PullRequestReviewHandler.new(json)

    assert_difference -> { KarmaEvent.count } => 0 do
      handler.handle
    end

    assert_nil handler.slack_message
  end

  # (duplicate approval)
  test 'does nothing if the event is invalid' do
    json = payload_mock 'github_pull_request_review'

    event_mock = Minitest::Mock.new
    event_mock.expect :invalid?, true

    handler = PullRequestReviewHandler.new(json)
    handler.stub(:persist_adjustment, event_mock) do
      assert_difference -> { KarmaEvent.count } => 0 do
        handler.handle
      end
    end

    assert_nil handler.slack_message
  end

  # See comment in class definition
  test 'does nothing if the review is a followup shell review' do
    json = payload_mock 'github_pull_request_shell_review'
    handler = PullRequestReviewHandler.new(json)

    assert_difference -> { KarmaEvent.count } => 0 do
      handler.handle
    end

    assert_nil handler.slack_message
  end
end
