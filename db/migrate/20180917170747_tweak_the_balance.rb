class TweakTheBalance < ActiveRecord::Migration[5.2]
  def up
    update "update karma_events set adjustment = -9 where event = 'pull_request_created'"
    update "update karma_events set adjustment = 9 where event = 'pull_request_retracted'"
  end

  def down
    update "update karma_events set adjustment = -10 where event = 'pull_request_created'"
    update "update karma_events set adjustment = 10 where event = 'pull_request_retracted'"
  end
end
