class PullRequestReviewJob < ApplicationJob
  include SlackPingable

  def perform(payload)
    @payload = payload

    # PR review wasn't counted if this returns nil
    return unless handler.handle

    # Comment karma events hopefully will have been created by now, find them and roll them up
    # into just one slack notification
    comment_count = associated_review_comments.length
    consolidated_adjustment = handler.event.adjustment + associated_review_comments.to_a.sum(&:adjustment)

    associate_child_comments
    build_and_send_notification comment_count, consolidated_adjustment
  end

  private

  def build_and_send_notification(comment_count, consolidated_adjustment)
    klc = KarmaLevelChange.new(handler.event.event,
                               consolidated_adjustment,
                               handler.event.handle,
                               handler.event.pr_title,
                               comment_count: comment_count)

    m = klc.build_change_notification
    slack.ping(m)
  end

  def associate_child_comments
    associated_review_comments.update_all(review_karma_event_id: handler.event.id)
  end

  def associated_review_comments
    @associated_review_comments ||= KarmaEvent.where(review_id: handler.event.review_id, event: :review_comment)
  end

  def handler
    @handler ||= PullRequestReviewHandler.new(@payload)
  end
end
