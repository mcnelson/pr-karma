class PullRequestHandler < KarmaEventHandler
  def handle
    if json.action.include?('opened')
      perform_adjustment_with_ping! json.pull_request.user.login,
                                    -9,
                                    :pull_request_created,
                                    json.pull_request

    elsif json.action == 'closed' && !json.pull_request.merged
      perform_adjustment_with_ping! json.pull_request.user.login,
                                    9,
                                    :pull_request_retracted,
                                    json.pull_request
    end
  end
end
