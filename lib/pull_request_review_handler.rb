class PullRequestReviewHandler < KarmaEventHandler
  def handle
    return if was_author_of_pr?

    if json.action == 'submitted' && json.review.state == "approved"
      perform_adjustment json.review.user.login,
                         4,
                         :review_approved,
                         json.pull_request,
                         review_id: json.review.id
    elsif json.action == 'submitted' && !followup_shell_review?
      perform_adjustment! json.review.user.login,
                          2,
                          :review_submitted,
                          json.pull_request,
                          review_id: json.review.id
    end
  end

  private

  def was_author_of_pr?
    json.review.user.login == json.pull_request.user.login
  end

  # Github pings us when someone replies to a PR review (as opposed to creating one anew). Not only with
  # a comment webhook but also with a review webhook, that is seemingly to a "shell" review, one that has
  # an HTML URL but when you go to it, it doesn't work.
  #
  # Studying the responses during testing, I observed that the review body is === null when it's a
  # "shell" review, and at least a "" otherwise.
  def followup_shell_review?
    json.review.body.nil?
  end
end
