json.array! @actions do |karma_event|
  json.repo karma_event.repo
  json.action karma_event.adjustment.positive? ? "review" : "opened_pr"
  json.pullRequestTitle karma_event.title
  json.change @relative_levels.relative_level(karma_event.adjustment)
  json.changeRelativeAmount @relative_levels.relative_level_index(karma_event.adjustment) + 1
  json.at karma_event.activity_at

  if karma_event.adjustment.positive?
    json.commentCount karma_event.comment_count
  end
end
