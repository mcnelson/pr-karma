json.array! @levels do |pair|
  json.handle pair.first
  json.karmaPosition @proportions.fetch(pair.first)
  json.level pair.last
end
