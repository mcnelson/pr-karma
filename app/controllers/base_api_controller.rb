class BaseApiController < ApplicationController
  before_action do
    if Rails.env.production? && request.remote_ip != ENV.fetch('ALLOWED_IP')
      render status: :forbidden
    end
  end
end
