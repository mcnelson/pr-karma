class PullRequestActionsController < BaseApiController
  def index
    @actions = query_pr_actions
    @relative_levels = RelativeAdjustmentLevel.new(@actions.map { |a| a.adjustment.abs })
  end

  private

  def query_pr_actions
    KarmaEvent.current.where(handle: params.fetch(:handle)).pull_request_actions
  end
end
