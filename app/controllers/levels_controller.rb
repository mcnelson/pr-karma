class LevelsController < BaseApiController
  def index
    balances = KarmaEvent.current.handles_and_balances

    kl = KarmaLevels.new(balances)
    @levels = kl.handles_and_levels
    @proportions = kl.handles_and_proportions
  end
end
