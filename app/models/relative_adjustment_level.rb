class RelativeAdjustmentLevel
  ADJUSTMENT_SEGMENTS = %w(slight medium hard).freeze

  def initialize(adjustments)
    @adjustments = adjustments
  end

  def relative_level(adjustment)
    ADJUSTMENT_SEGMENTS.fetch relative_level_index adjustment.abs
  end

  def relative_level_index(adjustment)
    value_ranges.index { |r| r.cover?(adjustment.abs) }
  end

  private

  def value_ranges
    @value_ranges ||= begin
                        low_stop = average / 2.0
                        high_stop = low_stop * 3.0

                        [0...low_stop, low_stop...high_stop, high_stop...nil]
                      end
  end

  def average
    @adjustments.to_a.sum / @adjustments.length.to_f
  end
end
